$(document).ready(()=> {

	var myPlayer = videojs('my-video').ready(function(){
		myPlayer = this;

		myPlayer.play();

	});

	$('a').click((event)=> {
	  event.preventDefault();
	  let src = event.target.getAttribute('href');
	  myPlayer.src({type: "video/mp4", src: src});
	  myPlayer.autoplay('play');

	  // Comment those lines if you want to disable auto-fullscreen on click
	  $('.vjs-play-control').click();
	  $('.vjs-fullscreen-control').click();

	  return -1;
	})

})